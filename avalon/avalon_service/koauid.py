import base64
from Crypto.Cipher import AES
import datetime
import sys, getopt

key = "4ca0415b0523e77092f5eccc9e5b07b1"

def uid_decrypt(token):
    token += (len(token) % 4) * "="
    uid = ""
    try:
        data = base64.urlsafe_b64decode(token)
        cipher = AES.new(key, AES.MODE_ECB)
        id = cipher.decrypt(data)
        uid = id[:-ord(id[len(id) - 1:])]
    finally:
        if uid.isdigit():
            return int(uid)
        return -1


def uid_encrypt(uid):
    cipher = AES.new(key, AES.MODE_ECB)
    padding = 16 - len(uid) % 16
    uid += padding * chr(padding)
    data = cipher.encrypt(uid)
    b64data = base64.urlsafe_b64encode(data).rstrip("=")
    return b64data


if __name__ == '__main__':
    uid = uid_encrypt(sys.argv[1])
    print uid
