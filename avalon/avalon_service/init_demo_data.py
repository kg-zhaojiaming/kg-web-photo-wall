#!/usr/bin/env python
import os
import uuid
from random import random
from admin_app import create_app
from modules import db
from modules.core import *
from werkzeug.security import generate_password_hash, check_password_hash
import datetime
from koauid import *

demos = 50
app = create_app(os.getenv('FLASK_CONFIG') or 'default')
images = [
    '01.jpg',
    '02.jpg',
    '03.jpg',
    '04.jpg',
    '05.jpg',
    '06.jpg',
]

def init_artwork():
    with app.app_context():
        db.drop_all()
        db.create_all()

        admin = AdminUser()
        admin.username = 'admin'
        admin.password = generate_password_hash('l1admin')
        db.session.add(admin)
        db.session.commit()

        cfg = Config()
        cfg.user_change_enable = True
        cfg.user_vote_interval = 120
        db.session.add(cfg)
        db.session.commit()

        today = datetime.datetime.now()
        day_ago = today - datetime.timedelta(days=2)
        for i in range(demos):
            user = GameUser()
            user.koa_uid = i
            user.koa_token = uid_encrypt(str(i))
            user.access_token = str(uuid.uuid1())
            user.last_vote_time = day_ago
            user.create_time = day_ago
            db.session.add(user)
        db.session.commit()
        idx = 0
        cnt = 0
        img_url = None
        for user in GameUser.query.all():
            img_url = '/static/img_demo/' + images[idx]
            idx = (idx + 1) % len(images)
            art = Artwork()
            art.user_id = user.id
            art.koa_uid = user.koa_uid
            art.image_url = img_url
            art.image_thumb_url = img_url
            art.approved_status = "approved"
            art.votes = cnt
            art.editor_votes = demos - cnt
            art.modify_time = today - datetime.timedelta(days=cnt)
            art.overwrite_enable = True
            user.artwork = art
            cnt = cnt + 1
            db.session.commit()

if __name__ == '__main__':
    init_artwork()
