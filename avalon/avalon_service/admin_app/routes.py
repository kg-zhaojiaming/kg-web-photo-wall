from flask import Blueprint
from modules import db
from modules.core import *
from views import *

from flask_admin import Admin, form
from flask_admin.form import rules
from flask_admin.contrib import sqla
from flask_admin.actions import action

def create_admin(app):
    # Create admin
    # admin = Admin(app, 'Avalon anniversary: Admin', template_mode='bootstrap3')
    init_login(app)
    admin = Admin(app, 'Avalon KOA', index_view=AdminIndexView(), base_template='rule_master.html')

    # Add views
    admin.add_view(GameUserView(GameUser, db.session))
    admin.add_view(ArtworkView(Artwork, db.session))
    admin.add_view(AdminUserView(AdminUser, db.session))
    admin.add_view(ConfigView(Config, db.session))
