from StringIO import StringIO
import csv
import codecs

import flask_admin as admin
import flask_login as login
from flask import current_app
from flask_admin.form import rules
from flask_admin.contrib import sqla
from flask_admin.actions import action
from flask_admin import helpers, expose
from flask_admin.model.template import EndpointLinkRowAction, LinkRowAction
from widgets.ExtendWidgets import NumberInputField
from flask import Flask, flash, redirect, render_template, \
     request, url_for
from wtforms import form, fields, validators
from jinja2 import Markup
from werkzeug.security import generate_password_hash, check_password_hash
from flask_admin.contrib.sqla import ModelView

from modules import db
from modules.core import *

from flask import make_response, Response
from flask_admin import tools
from flask_admin.tools import rec_getattr
from flask_admin.contrib.sqla.filters import BaseSQLAFilter, FilterEqual
from sqlalchemy.event import listens_for
import requests
from md5 import md5

class BaseModelView(ModelView):
    export_columns = None

    # Exporting
    def _get_data_for_export(self):
        view_args = self._get_list_extra_args()

        # Map column index to column name
        sort_column = self._get_column_by_idx(view_args.sort)
        if sort_column is not None:
            sort_column = sort_column[0]

        _, query = self.get_list(view_args.page, sort_column, view_args.sort_desc, view_args.search,
                                 view_args.filters, execute=False)

        return query.limit(None).all()

    def get_export_csv(self):
        self.export_columns = self.export_columns or [column_name for column_name, _ in self._list_columns]

        io = StringIO()
        io.write(codecs.BOM_UTF8)
        rows = csv.DictWriter(io, self.export_columns)

        data = self._get_data_for_export()

        rows.writeheader()

        for item in data:
            row = {column: unicode(rec_getattr(item, column)).encode('utf-8') for column in self.export_columns}
            rows.writerow(row)

        io.seek(0)
        return io.getvalue()

    @expose('/export/')
    def export(self):
        response = make_response(self.get_export_csv())
        response.mimetype = 'text/csv'
        response.headers['Content-Disposition'] = 'attachment; filename=%s.csv' % self.name.lower().replace(' ', '_')

        return response

    def scaffold_list_columns(self):
        """
            Return a list of columns from the model.
        """
        columns = []

        for p in self._get_model_iterator():
            if hasattr(p, 'direction'):
                if self.column_display_all_relations or p.direction.name == 'MANYTOONE':
                    columns.append(p.key)
            elif hasattr(p, 'columns'):
                if len(p.columns) > 1:
                    filtered = tools.filter_foreign_columns(self.model.__table__, p.columns)

                    if len(filtered) == 0:
                        continue
                    elif len(filtered) > 1:
                        warnings.warn('Can not convert multiple-column properties (%s.%s)' % (self.model, p.key))
                        continue

                    column = filtered[0]
                else:
                    column = p.columns[0]

                if not self.column_display_fk and column.foreign_keys:
                    continue

                if not self.column_display_pk and column.primary_key:
                    continue

                columns.append(p.key)

        return columns


def init_login(app):
    login_manager = login.LoginManager()
    login_manager.init_app(app)

    @login_manager.user_loader
    def load_user(user_id):
        return db.session.query(AdminUser).get(user_id)


class LoginForm(form.Form):
    login = fields.StringField(validators=[validators.required()],
            render_kw={"placeholder": "Username"})
    password = fields.PasswordField(validators=[validators.required()],
            render_kw={"placeholder": "Password"})

    def validate_login(self, field):
        user = self.get_user()

        if user is None:
            raise validators.ValidationError('Invalid user')

        if not check_password_hash(user.password, self.password.data):
            raise validators.ValidationError('Invalid password')

    def get_user(self):
        return AdminUser.query.filter_by(username=self.login.data).first()


class AdminIndexView(admin.AdminIndexView):

    @expose('/')
    def index(self):
        if not login.current_user.is_authenticated:
            return redirect(url_for('.login_view'))
        return super(AdminIndexView, self).index()

    @expose('/login/', methods=('GET', 'POST'))
    def login_view(self):
        # handle user login
        form = LoginForm(request.form)
        if helpers.validate_form_on_submit(form):
            user = form.get_user()
            login.login_user(user)

        if login.current_user.is_authenticated:
            return redirect(url_for('.index'))
        self._template_args['form'] = form
        return super(AdminIndexView, self).index()

    @expose('/logout/')
    def logout_view(self):
        login.logout_user()
        return redirect(url_for('.index'))

class AdminUserView(BaseModelView):
    def is_accessible(self):
        return login.current_user.is_authenticated

    column_display_pk = True
    can_create = False
    can_delete = False
    can_view_details = True
    can_export = True
    can_edit = False
    column_display_fk = True
    can_view_details = True


class GameUserView(BaseModelView):
    def is_accessible(self):
        return login.current_user.is_authenticated

    column_searchable_list = ['id','koa_uid']
    column_filters = ['id', 'koa_uid']
    column_display_pk = True
    can_create = False
    can_delete = False
    can_view_details = False
    can_export = True
    can_edit = False
    column_display_fk = True
    can_view_details = True

class ConfigView(BaseModelView):
    def is_accessible(self):
        return login.current_user.is_authenticated

    column_editable_list = ['user_change_enable', 'user_vote_interval']
    can_create = False
    can_delete = False
    can_view_details = False
    can_edit = True
    column_display_fk = False

    form_choices = {
        'user_change_enable': [
            ('1', 'True'),
            ('0', 'False'),
            ]
        }

@listens_for(Config, 'after_update')
def update_timestamp(mapper, conn, target):
    for server in current_app.config['API_SERVERS']:
        url = server + '/api/config'
        print 'update config to:', url
        requests.post(url)
    print 'update finished'



# Create custom filter class
class FilterArtworkStatus(BaseSQLAFilter):
    def apply(self, query, value, alias=None):
        if value == '0':
            return query.filter(self.column == "pending")
        elif value == '1':
            return query.filter(self.column == "approved")
        else:
            return query.filter(self.column == "rejected")

    def operation(self):
        return 'Status is'

class ArtworkView(BaseModelView):
    def is_accessible(self):
        return login.current_user.is_authenticated
    # Use same rule set for edit page
    create_template = 'rule_create.html'
    edit_template = 'rule_edit.html'
    list_template = 'rule_list.html'

    column_searchable_list = ['id']
    column_filters = [
            FilterEqual(column=Artwork.id, name='Id'),
            FilterArtworkStatus(column=Artwork.approved_status, name="Status",
                options=(
                    ('0','pending'),
                    ('1', 'approved'),
                    ('2','rejected')))
            ]

    column_display_pk = True
    can_create = False
    can_delete = False
    can_view_details = True
    can_export = True
    can_edit = False
    column_display_fk = True
    column_list = ['id','koa_uid', 'game_user.kingdom', 'game_user.lord_name', 'approved_status', 'votes', 'editor_votes', 'image_url', 'modify_time','overwrite_enable']
    column_labels = {'game_user.kingdom': 'kingdom','game_user.lord_name':'lord_name'}
    column_exclude_list=['game_user']
    column_default_sort = ('modify_time', True)

    @action('approve', 'Approve', 'Are you sure you want to approve selected artworks?')
    def action_approve(self, ids):
        try:
            query = Artwork.query.filter(Artwork.id.in_(ids))
            count = 0
            for artwork in query.all():
                artwork.approve()
                count += 1
            flash('%s artwork(s) were successfully approved.' %  count)
        except Exception as ex:
            if not self.handle_view_exception(ex):
                raise
            flash('Failed to approve artworks. %s' % str(ex))

    @action('reject', 'Reject', 'Are you sure you want to reject selected artworks?')
    def action_reject(self, ids):
        try:
            query = Artwork.query.filter(Artwork.id.in_(ids))
            count = 0
            for artwork in query.all():
                artwork.reject()
                count += 1
            flash('%s artwork(s) were successfully rejected.' %  count)
        except Exception as ex:
            if not self.handle_view_exception(ex):
                raise
            flash('Failed to reject artworks. %s' % str(ex))

    def _list_url(view, context, model, name):
        url = model.image_url + '?v=' + model.version()
        print "url",url
        link = '<a href="%s" target="_blank"><img height="50" width="50" src="%s" hint="%s" alt="" class="artimg" /></a>' % (url, url, url)
        return Markup(link)

    column_editable_list = ['votes', 'editor_votes', 'approved_status', 'overwrite_enable']

    column_formatters = {
        'image_url': _list_url,
        'image_thumb_url': _list_url
    }

    form_choices = {
    'approved_status': [
        ('pending', 'pending'),
        ('approved', 'approved'),
        ('rejected', 'rejected')
        ],
    'overwrite_enable': [
        ('1', 'True'),
        ('0', 'False'),
        ]    
    }
