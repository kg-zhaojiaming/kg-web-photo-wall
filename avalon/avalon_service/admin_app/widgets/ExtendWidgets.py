
from wtforms.widgets import Input
from wtforms import IntegerField

class NumberInputWidget(Input):
    input_type = 'number'

    def __call__(self, field, **kwargs):
        return super(NumberInputWidget, self).__call__(field, **kwargs)

class NumberInputField(IntegerField):
    widget = NumberInputWidget()
