from modules import db
from datetime import datetime

from flask import Flask, url_for
from flask_sqlalchemy import SQLAlchemy
from flask import Flask, url_for
from md5 import md5

class Config(db.Model):
    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    user_vote_interval = db.Column(db.Integer, nullable=True)
    user_change_enable = db.Column(db.Integer, nullable=True)

    @property
    def is_authenticated(self):
        return True

    @property
    def is_active(self):
        return True

    @property
    def is_anonymous(self):
        return False

class AdminUser(db.Model):
    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    username = db.Column(db.Unicode(128), unique=True, nullable=False)
    password = db.Column(db.Unicode(128), nullable=False)
    create_time = db.Column(db.DateTime, default=datetime.now)
    modify_time = db.Column(db.DateTime, default=datetime.now)
    @property
    def is_authenticated(self):
        return True

    @property
    def is_active(self):
        return True

    @property
    def is_anonymous(self):
        return False

    def get_id(self):
        return self.id

    # Required for administrative interface
    def __unicode__(self):
        return self.username


class GameUser(db.Model):
    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    koa_uid = db.Column(db.Unicode(128), unique=True, nullable=False)
    koa_token = db.Column(db.Unicode(128), unique=True, nullable=False)
    access_token = db.Column(db.Unicode(128), unique=True, nullable=False)
    kingdom = db.Column(db.Unicode(128), nullable=True)
    lord_name = db.Column(db.Unicode(128), nullable=True)
    create_time = db.Column(db.DateTime, default=datetime.now)
    last_vote_time = db.Column(db.DateTime, default=datetime.now)
    last_vote_art_id = db.Column(db.Integer, nullable=True)
    artwork = db.relationship("Artwork", uselist=False, back_populates="game_user")
    def __repr__(self):
        return '%s' % (self.koa_uid)


class Artwork(db.Model):
    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    koa_uid = db.Column(db.Unicode(128), unique=True, nullable=False)
    user_id = db.Column(db.Integer, db.ForeignKey('game_user.id'), nullable=False)
    votes = db.Column(db.Integer, default=0, nullable=False)
    editor_votes = db.Column(db.Integer, default=0, nullable=False)
    image_url = db.Column(db.Unicode(256), nullable=True)
    image_thumb_url = db.Column(db.Unicode(256), nullable=True)

    game_user = db.relationship("GameUser", back_populates="artwork")

    def __repr__(self):
        return '%s' % self.id

    """
    status pending
    status approved
    status rejected
    """
    approved_status = db.Column(db.Unicode(64), default=u'pending', nullable=False)
    kingdom = db.Column(db.Unicode(128), nullable=True)
    lord_name = db.Column(db.Unicode(128), nullable=True)

    create_time = db.Column(db.DateTime, default=datetime.now)
    modify_time = db.Column(db.DateTime, default=datetime.now)
    overwrite_enable = db.Column(db.Integer, nullable=False)

    def version(self):
        return md5(str(self.modify_time)).hexdigest()

    def approve(self):
        self.approved_status = "approved"
        db.session.commit()

    def reject(self):
        self.approved_status = "rejected"
        db.session.commit()
