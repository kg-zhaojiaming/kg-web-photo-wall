from flask import Blueprint
from flask import request
from flask import make_response, Response
from flask import jsonify
from sqlalchemy.sql import and_, desc
from sqlalchemy.orm import aliased
from modules import db
from modules.core import Artwork
from modules.core import GameUser
from modules.core import Config
from flask import current_app

import uuid
import datetime
import json
from config import config
from koauid import uid_decrypt
from md5 import md5

RC_OK = 0
RC_INVALID_ACCESS_TOKEN = -1
RC_INVALID_USER_TOKEN = -2
RC_INVALID_ARTWORK = -3
RC_VOTED = -4
RC_INTERNAL_ERROR = -5
RC_OVERWRITE_DISABLE = -6

SEARCH_PAGE_LIMIT = 20

URL_BASE = "http://d2oicj4h40q6d5.cloudfront.net"

api_routes = Blueprint('api', __name__)


def get_artwork_id(req_art_id):
    if req_art_id.isdigit():
        req_art_id = req_art_id[1:]
    else:
        req_art_id = "-1"
    return int(req_art_id)


def is_validated_access_token(token):
    user = GameUser.query.filter_by(access_token=token).first()
    return user


def is_validated_user_token(token):
    user = GameUser.query.filter_by(user_token=token).first()
    if user != None:
        return True
    return False


def is_validated_user_and_voteable(token):
    current_time = datetime.datetime.now()
    if current_app.config['SYSTEM_VOTE_DEBUG']:
        return True
    day_ago = current_time - datetime.timedelta(days=current_app.config['SYSTEM_VOTE_INTERVAL_DAY'])
    user = GameUser.query.filter(GameUser.last_vote_time < day_ago, GameUser.access_token == token).first()
    if user is not None:
        return True
    return False


def format_artwork(art):
    return {'art_id': art.id,
            'image_url': art.image_url,
            'image_thumb_url': art.image_thumb_url,
            'votes': art.votes}

def conv_url(url, version):
    #new_url = "%s?v=%s" % (url, version)
    new_url = "%s/%s?v=%s" % (URL_BASE, url[url.rfind("/")+1:], version)
    return new_url

@api_routes.route('/list', methods=['POST'])
def list_arts():
    """
    api args
    access_token: the validate user's access token
    type = editor,votes,time
    page = page start
    """

    list_type = request.form.get("type", type=str, default='editor')
    page = request.form.get("page", type=int, default=0)
    req_access_token = request.form.get("access_token", type=str, default='0')

    user = is_validated_access_token(req_access_token)
    if user is None:
        return jsonify(rc=RC_INVALID_ACCESS_TOKEN)

    if list_type == 'editor':
        sort_type = Artwork.editor_votes
    elif list_type == 'votes':
        sort_type = Artwork.votes
    elif list_type == 'time':
        sort_type = Artwork.modify_time
    else:
        sort_type = Artwork.editor_votes

    offset = page * SEARCH_PAGE_LIMIT
    limit = SEARCH_PAGE_LIMIT
    GameUserAliased = aliased(GameUser)
    art_query = db.session.query(
        Artwork.id,
        Artwork.user_id,
        Artwork.image_url,
        Artwork.image_thumb_url,
        Artwork.votes,
        GameUser.last_vote_time,
        GameUserAliased.kingdom,
        GameUserAliased.lord_name,
        Artwork.modify_time,
    ).join(
        GameUser,
        and_(Artwork.id == GameUser.last_vote_art_id, GameUser.id == user.id),
        isouter=True
    ).join(
        GameUserAliased,
        Artwork.user_id == GameUserAliased.id,
        isouter=True
    ).filter(Artwork.approved_status=='approved').order_by(desc(sort_type)).offset(offset).limit(limit)

    arts = art_query.all()
    art_list = []
    for art in arts:
        voted = False if art[5] is None else art[5].date() == datetime.datetime.now().date()
        version = md5(str(art[8])).hexdigest()
        art_list.append({
            'art_id': art[0],
            'user_id': art[1],
            'image_url': conv_url(art[2], version),
            'image_thumb_url': conv_url(art[3], version),
            'votes': art[4],
            'voted': voted,
            'kingdom': art[6],
            'lord_name': art[7]
        })

    return jsonify(arts=art_list, rc=RC_OK)


@api_routes.route('/search', methods=['POST'])
def search_arts():
    """
    api args
    access_token: the validate user's access token
    art_id = the artwork serial number (etc: '1')
    """

    req_access_token = request.form.get("access_token", type=str, default='0')
    user = is_validated_access_token(req_access_token)
    if user is None:
        return jsonify(rc=RC_INVALID_ACCESS_TOKEN)

    req_art_id = request.form.get("art_id", type=int, default=-1)
    GameUserAliased = aliased(GameUser)
    art_query = db.session.query(
        Artwork.id,
        Artwork.user_id,
        Artwork.image_url,
        Artwork.image_thumb_url,
        Artwork.votes,
        GameUser.last_vote_time,
        GameUserAliased.kingdom,
        GameUserAliased.lord_name,
        Artwork.modify_time,
    ).join(
        GameUser,
        and_(Artwork.id == GameUser.last_vote_art_id, GameUser.id == user.id),
        isouter=True
    ).join(
        GameUserAliased,
        Artwork.user_id == GameUserAliased.id,
        isouter=True
    ).filter(Artwork.id == req_art_id, Artwork.approved_status == 'approved')
    art = art_query.first()
    if art is None:
        return jsonify(arts={}, rc=RC_OK)

    voted = False if art[5] is None else art[5].date() == datetime.datetime.now().date()
    version = md5(str(art[8])).hexdigest()

    result = {
        'art_id': art[0],
        'user_id': art[1],
        'image_url':conv_url(art[2], version),
        'image_thumb_url': conv_url(art[3], version),
        'votes': art[4],
        'voted': voted,
        'kingdom': art[6],
        'lord_name': art[7]
    }

    return jsonify(arts=result, rc=RC_OK)


@api_routes.route('/upload', methods=['POST'])
def upload_art():
    """
    api args
    access_token: the validate user's access token
    img_url: the URL of the new image
    img_thumb_url: the URL of the thumb to the new image
    """

    req_access_token = request.form.get("access_token", type=str, default='0')
    req_img_url = request.form.get("img_url", type=str, default='0')
    req_img_thumb_url = request.form.get("img_thumb_url", type=str, default='0')

    #check the token first
    user = GameUser.query.filter_by(access_token=req_access_token).first()
    if user != None:
        if user.artwork == None:
            art = Artwork()
            art.approved_status = "pending"
            art.image_url = req_img_url
            art.image_thumb_url = req_img_thumb_url
            art.kingdom = user.kingdom
            art.lord_name = user.lord_name
            art.koa_uid = user.koa_uid
        else:
            art = user.artwork
            art.approved_status = "pending"
            art.image_url = req_img_url
            art.image_thumb_url = req_img_thumb_url
            art.modify_time = datetime.datetime.now()
            art.vots = 0
            art.editor_votes = 0
            art.kingdom = user.kingdom
            art.lord_name = user.lord_name
        art.overwrite_enable = False
        user.artwork = art
        db.session.commit()
        return jsonify(artical_id=art.id, rc=RC_OK)
    else:
        return jsonify(rc=RC_INVALID_ACCESS_TOKEN)


@api_routes.route('/vote', methods=['POST'])
def vote_arts():
    """
    api args
    access_token: the validate user's access token
    art_id: the URL of the new image
    """
    req_access_token = request.form.get("access_token", type=str, default='0')
    req_art_id = request.form.get("art_id", type=int, default=-1)
    user = is_validated_access_token(req_access_token)
    if user is None:
        return jsonify(rc=RC_INVALID_ACCESS_TOKEN)
    if not is_validated_user_and_voteable(req_access_token):
        return jsonify(rc=RC_VOTED)
    art = Artwork.query.filter_by(id=req_art_id).first()
    if art is not None:
        art.votes = art.votes + 1
        user.last_vote_time = datetime.datetime.now()
        user.last_vote_art_id = art.id
        db.session.commit()
        return jsonify(art=format_artwork(art), rc=RC_OK)
    else:
        return jsonify(c=RC_INVALID_ARTWORK)

@api_routes.route('/check', methods=['POST'])
def upload_check():
    """
    api args
    access_token: the validate user's access token
    """
    req_access_token = request.form.get("access_token", type=str, default='0')
    user = GameUser.query.filter_by(access_token=req_access_token).first()

    if user is not None:
        if user.artwork is not None:
            art = user.artwork
            if art.overwrite_enable:
                return jsonify(rc=RC_OK)
            else:
                return jsonify(rc=RC_OVERWRITE_DISABLE)
        else:
            return jsonify(rc=RC_OK)
    else:
        return jsonify(rc=RC_INVALID_ACCESS_TOKEN)


@api_routes.route('/home', methods=['POST'])
def user_home():
    """
    api args
    access_token: the validate user's access token
    """

    req_access_token = request.form.get("access_token", type=str, default='0')
    user = GameUser.query.filter_by(access_token=req_access_token).first()

    if user is not None:
        if user.artwork is not None:
            art = user.artwork
            voted = False
            if user.last_vote_art_id == art.id and \
                    datetime.datetime.now().date() == user.last_vote_time.date():
                voted = True
            version = md5(str(art.modify_time)).hexdigest()
            return jsonify(
                arts={
                    'art_id': art.id,
                    'img_url': conv_url(art.image_url, version),
                    'img_thumb_url': conv_url(art.image_thumb_url, version),
                    'approved_status': art.approved_status,
                    'votes': art.votes,
                    'voted': voted,
                },
                rc=RC_OK
            )
        else:
            return jsonify(arts={}, rc=RC_OK)
    else:
        return jsonify(rc=RC_INVALID_ACCESS_TOKEN)


@api_routes.route('/params', methods=['POST'])
def get_params():
    """
    api args
    access_token: the validate user token
    """

    req_access_token = request.form.get("access_token", type=str, default='0')
    user = is_validated_access_token(req_access_token)
    if user is None:
        return jsonify(rc=RC_INVALID_ACCESS_TOKEN)
    overwrite_enable = True
    if user.artwork is not None:
        overwrite_enable = user.artwork.overwrite_enable > 0

    ret = {'s3_bucket': current_app.config['S3_BUCKET'],
           's3_bucket_region': current_app.config['S3_BUCKET_REGION'],
           's3_pool_id': current_app.config['S3_IDENTITY_POOL_ID'],
           'change_enable': current_app.config['USER_CHANGE_ENABLE'],
           'upload_interval': current_app.config['USER_VOTE_INTERVAL'],
           'overwrite_enable': overwrite_enable}
    return jsonify(params=ret, rc=RC_OK)

@api_routes.route('/init', methods=['POST'])
def init_user():
    """
    api args
    user_token: the validate user token
    kingdom:
    lord_name:
    """
    req_user_token = request.form.get("user_token", type=str, default='0')
    kingdom = request.form.get("kingdom", default=u'0')
    lord_name = request.form.get("lord_name", default=u'0')
    uid = uid_decrypt(req_user_token)
    if uid == -1:
        return jsonify(rc=RC_INVALID_USER_TOKEN)
    user = GameUser.query.filter_by(koa_uid=uid).first()
    if user is not None:
        user.kingdom = kingdom
        user.lord_name = lord_name
        db.session.add(user)
        db.session.commit()
        return jsonify(access_token=user.access_token, rc=RC_OK)
    else:
        today = datetime.datetime.now()
        day_ago = today - datetime.timedelta(days=2)
        user = GameUser()
        user.koa_uid = uid
        user.koa_token = req_user_token
        user.access_token = unicode(uuid.uuid1())
        user.last_vote_time = day_ago
        user.create_time = day_ago
        user.kingdom = kingdom
        user.lord_name = lord_name
        db.session.add(user)
        db.session.commit()
        return jsonify(access_token=user.access_token, rc=RC_OK)

@api_routes.route('/config', methods=['POST'])
def update_config():
    config = Config.query.first()
    if config is None:
        return jsonify(rc=RC_INTERNAL_ERROR)
    current_app.config.update(
            USER_CHANGE_ENABLE = False if config.user_change_enable == 0 else True,
            USER_VOTE_INTERVAL = config.user_vote_interval)
    return jsonify(rc=RC_OK)

def update_config_fromdb(app):
    with app.app_context():
        config = Config.query.first()
        if config is None:
            return RC_INTERNAL_ERROR
        app.config.update(
                USER_CHANGE_ENABLE = False if config.user_change_enable == 0 else True,
                USER_VOTE_INTERVAL = config.user_vote_interval)
        return RC_OK

def create_api(app):
    update_config_fromdb(app)
    app.register_blueprint(api_routes,url_prefix='/api')
