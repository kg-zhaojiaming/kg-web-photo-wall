export FLASK_CONFIG=dev
#export FLASK_CONFIG=debug
#export FLASK_CONFIG=production
nohup gunicorn -b 127.0.0.1:8000 -w 1 api_app.wsgi > api.output 2>&1 &
nohup gunicorn -b 127.0.0.1:8001 -w 1 admin_app.wsgi > admin.output 2>&1 &
