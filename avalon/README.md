# 环境准备
## 1. 创建部署账户 koaos
adduser koaos
将koaos 用户添加为sudoers

编辑/etc/sudoers文件
sudo vim /etc/sudoers
添加如下行
koaos ALL=(ALL)       ALL

接下来所有的步骤都使用koaos用户进行

## 2. 安装 python相关依赖
curl https://bootstrap.pypa.io/get-pip.py -o get-pip.py
sudo python get-pip.py
sudo pip install virtualenv --prefix=/usr
sudo pip install supervisor --prefix=/usr

cd /home/koaos
virtualenv venv

## 3. 安装mysql client
sudo yum install mysql-devel

## 4. 安装 nvm node js
curl -o- https://raw.githubusercontent.com/creationix/nvm/v0.33.0/install.sh | bash
重新登录后运行 nvm install --lts

## 5. 安装nginx
sudo yum install nginx

# 部署
将项目目录拷贝到 /home/koaos/avalon

## 1. 编译H5应用
cd /home/koaos/avalon/avalon_app
npm run build

打包:
tar -cvf dist.tar dist

主意:可以使用已经打包好的包，安装方法如下
cd /home/koaos/avalon/avalon_app
tar zxf ../dist.tar.gz


## 2. 配置后台应用
cd /home/koaos/avalon/avalon_service
编辑 config.py 文件

修改S3配置至正确配置
S3_BUCKET = ''
S3_BUCKET_REGION = ''
S3_IDENTITY_POOL_ID = ''

将服务器信息填写到下边例如
API_SERVERS = ['http://10.11.106.186','http://10.11.106.147']

修改Mysql数据库连接字符串
SQLALCHEMY_DATABASE_URI = 'mysql://username:password@hostname:port/dbname?charset=utf8mb4'

## 3. 配置nginx
cp nginx.aws.http.conf /etc/nginx/nginx.conf
服务使用80端口，请确保防火墙配置正确
sudo service nginx start

## 4. 初始化应用
source ~/venv/bin/activate
cd /home/koaos/avalon/avalon_service
安装必要的python库
pip install -r requirements.txt
初始化空数据库(搭建负载均衡集群只需要初始化一次)
export FLASK_CONFIG=production
python init_db_data.py

下边这命令用于初始化一个演示数据库。请注意，演示数据库和正常的数据库都是使用同一个数据库。每次运行命令都会清除之前的数据库数据
export FLASK_CONFIG=production
python init_demo_data.py

## 5. 部署应用自动启动
cp /home/koaos/avalon/supervisord /etc/init.d/supervisord
cp /home/koaos/avalon/supervisord.conf /etc/
sudo service supervisord start
sudo service supervisord status
