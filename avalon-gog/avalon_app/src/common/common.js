var S3 = require('aws-sdk/clients/s3')
var AWS = require('aws-sdk/global')
var blueimp = require('blueimp-canvas-to-blob')

import md5 from 'md5'

export function isEmpty(strings) {
  return (
    typeof strings === "undefined"
    || strings === null
    || strings.match(/^\s*$/)
  )
}

export function getKey(str) {
  let user_md5 = md5(str)
  let len = str.length
  let exStr
  if (len < 5) {
    exStr = '8f3'
  } else {
    exStr = str.substring(len - 5, len - 2)
  }
  let key = exStr + user_md5 + exStr + exStr.substring(2, 3)
  return key
}

function getParameterByName(name, url) {
  if (!url) {
    url = window.location.href
  }
  name = name.replace(/[\[\]]/g, "\\$&");
  var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
    results = regex.exec(url)
  if (!results) return null
  if (!results[2]) return ''
  return decodeURIComponent(results[2].replace(/\+/g, " "))
}

function dataURLtoBlob(dataurl) {
  let arr = dataurl.split(','), mime = arr[0].match(/:(.*?);/)[1],
    bstr = atob(arr[1]), n = bstr.length, u8arr = new Uint8Array(n)
  while (n--) {
    u8arr[n] = bstr.charCodeAt(n)
  }
  return new Blob([u8arr], { type: mime })
}

export function NewS3Conntection(bucket, region, poolId) {
  let albumBucketName = bucket
  let bucketRegion = region
  let IdentityPoolId = poolId

  AWS.config.update({
    region: bucketRegion,
    credentials: new AWS.CognitoIdentityCredentials({
      IdentityPoolId: IdentityPoolId
    })
  })

  let s3 = new S3({
    apiVersion: '2006-03-01',
    params: {Bucket: albumBucketName},
    region: region
  })
  return s3
}

// callback(err, data)
export function upload_image(callback, s3, file, userKey) {
  let filename = file.name
  let idx = filename.lastIndexOf(".")
  let ext = filename.substring(idx)
  let photoKey = 'gog2rd/' + userKey + '.jpeg'
  compress_image(
    blobImage => {
      s3.upload({
        Key: photoKey,
        Body: blobImage,
        ACL: 'public-read',
        ContentType: "image/jpeg"
      }, callback)
    },
    file
  )
}

// callback(blobImage)
function compress_image(callback, file) {
  let image_url = ''
  let blob = URL.createObjectURL(file)
  let img = new Image()
  img.src = blob

  img.onload = function() {
    let that = this

    let w = that.width
    let h = that.height
    let scale = 1
    if (that.height > 1024 && that.width > 1024) {
      scale = 1024 / Math.max(that.height, that.width)
      h = h * scale
      w = w * scale
    }
    else if (that.width > 1024) {
      scale = w / h
      w = 1024 || w
      h = w / scale
    }
    else if (that.height > 1024) {
      scale = h / w
      h = 1024 || h
      w = h / scale
    }

    let canvas = document.createElement('canvas')
    let ctx = canvas.getContext('2d')
    canvas.height = h
    canvas.width = w
    ctx.drawImage(that, 0, 0, w, h)
    if (navigator.userAgent.match(/iphone/i)) {
      let mpImg = new MegaPixImage(img)
      mpImg.render(canvas, {
        maxWidth: w,
        maxHeight: h,
        quality: 1
      })
      canvas.toBlob(callback, "image/jpeg", 0.7)
    }
    else if (navigator.userAgent.match(/Android/i)) {
      let encoder = new JPEGEncoder()
      let base64 = encoder.encode(ctx.getImageData(0, 0, w, h), 0.8 * 100 || 80)
      callback(dataURLtoBlob(base64))
    }
    else {
      canvas.toBlob(callback, "image/jpeg", 0.8)
    }
  }
}
