import os
basedir = os.path.abspath(os.path.dirname(__file__))

class Config:
    SECRET_KEY = os.environ.get('SECRET_KEY') or '123456790'
    SQLALCHEMY_COMMIT_ON_TEARDOWN = True
    SQLALCHEMY_TRACK_MODIFICATIONS = False
    SYSTEM_VOTE_INTERVAL_DAY = 1
    API_SERVERS = ['http://127.0.0.1:8080']
    ## The config below may overwrite by db config
    USER_CHANGE_ENABLE = True
    USER_VOTE_INTERVAL = 120 # seconds

    @staticmethod
    def init_app(app):
        pass

class DevelopmentConfig(Config):
    DEBUG = False
    SQLALCHEMY_ECHO = False
    SYSTEM_VOTE_DEBUG = False
    S3_BUCKET = 'avalonimage'
    S3_BUCKET_REGION = 'us-east-1'
    S3_IDENTITY_POOL_ID = 'us-east-1:1550d78c-5453-47e8-b30e-da1fe7beda53'
    SQLALCHEMY_DATABASE_URI = os.environ.get('DEV_DATABASE_URL') or \
    'sqlite:///' + os.path.join(basedir, 'data-dev.sqlite')

class DevelopmentConfigDebug(Config):
    DEBUG = True
    SQLALCHEMY_ECHO = True
    SYSTEM_VOTE_DEBUG = True
    S3_BUCKET = 'avalonimage'
    S3_BUCKET_REGION = 'us-east-1'
    S3_IDENTITY_POOL_ID = 'us-east-1:1550d78c-5453-47e8-b30e-da1fe7beda53'
    SQLALCHEMY_DATABASE_URI = os.environ.get('DEV_DATABASE_URL') or \
    'sqlite:///' + os.path.join(basedir, 'data-dev.sqlite')

class ProductionConfig(Config):
    SYSTEM_VOTE_DEBUG = False
    S3_BUCKET = 'koa-annual-activity'
    S3_BUCKET_REGION = 'us-west-2'
    S3_IDENTITY_POOL_ID = 'us-west-2:2af8f467-09cd-42a0-8b3f-c573088d4fc2'
    SQLALCHEMY_DATABASE_URI = 'mysql://koasiteos:KGwebwszzh6s@dw-global-spam.cgdvjhtbmo8n.us-east-1.rds.amazonaws.com:3306/koadb?charset=utf8mb4'
    API_SERVERS = ['http://10.11.106.186','http://10.11.106.147']

config = {
    'dev': DevelopmentConfig,
    'debug': DevelopmentConfigDebug,
    'production': ProductionConfig,
    'default': DevelopmentConfig
}
