# -*- coding: utf-8 -*-
import os

from . import create_app

application = app = create_app(os.getenv('FLASK_CONFIG') or 'default')
