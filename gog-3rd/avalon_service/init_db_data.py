#!/usr/bin/env python
import os
import uuid
from random import random
from admin_app import create_app
from modules import db
from modules.core import Artwork
from modules.core import GameUser
from modules.core import AdminUser
from modules.core import Config
from werkzeug.security import generate_password_hash, check_password_hash
import datetime
import sys, getopt

app = create_app(os.getenv('FLASK_CONFIG') or 'default')

def init_artwork():
    with app.app_context():
        db.drop_all()
        db.create_all()
        admin = AdminUser()
        admin.username = u'admin'
        admin.password = unicode(generate_password_hash('l1admin'))
        db.session.add(admin)
        db.session.commit()

        cfg = Config()
        cfg.user_change_enable = True
        cfg.user_vote_interval = 120
        db.session.add(cfg)
        db.session.commit()


if __name__ == '__main__':
    init_artwork()
